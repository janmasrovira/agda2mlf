# Introduction
<!-- Describe scope and aim of project -->

# Implementation
<!-- Describe design-considerations and challenges and limitations (backlog) -->

# Red/Black-tree
<!-- Briefly describe red-black algorithm and give a brief overview of
how dependent types can be helpful in this domain. -->
<!-- Compare the different implementations -->

# Analysis
<!-- Describe tests and test-results, compare result from Agda backend
and the Haskell versions that to varying degree try to emulate some
dependent-types -->

# Discussion
<!-- What have we learned. Is the compiler we wrote the one to rule the all? -->

# Perspectives
<!-- What further work could be done? -->

