{-# LANGUAGE BangPatterns #-}
module Extra where

blumblumshub :: Int -> Int -> Int
blumblumshub xn = mod (xn ^ (2 :: Int))

downFromBbs' :: Int -> Int -> [Int]
downFromBbs' x0 = f x0 []
  where
    f _ acc 0       = acc
    f !x !acc l = f (blumblumshub x m) (x : acc) (l - 1)
      where
        m           = p * q
        p         = 2971
        q         = 4111

downFrom' :: Int -> [Int]
downFrom' n = f [] n
  where
    f xs 0 = xs
    f !xs x = let nx = n - x in f (nx `seq` nx : xs) (x - 1)

fromToBbs :: Int -> Int -> [Int]
fromToBbs _ 0 = []
fromToBbs xi n = xi : fromToBbs (blumblumshub xi m) (n - 1)
  where
    m         = p * q
    p         = 2971
    q         = 4111


fromTo :: Int -> [Int]
fromTo n = f n
  where
    f 0 = []
    f x = n - x + 1 : f (x - 1)

-- [n, n-1..1]
downFrom :: Int -> [Int]
downFrom 0 = []
downFrom n = n : downFrom (n - 1)

fromTo' :: Int -> [Int]
fromTo' = f []
  where
    f xs 0 = xs
    f !xs x = f (x : xs) (x - 1)

